/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servico.model;

/**
 *
 * @author alexandre
 */

public class Produto {
    
    private String nome;
    private String descricao;
    private double valorCompra;
    private double valorVenda;
    private double quantidadeEstoque;

   
    public void setNome(String nome) {
        this.nome = nome;
    }

   
    public String getDescricao() {
        return descricao;
    }

    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    
    public double getValorCompra() {
        return valorCompra;
    }

    
    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }

    
    public double getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(double valorVenda) {
        this.valorVenda = valorVenda;
    }

    
    public double getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    
    public void setQuantidadeEstoque(double quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
    public Produto(String nome, double valorCompra, double quantidadeEstoque){
        this.nome = nome;
        this.valorCompra = valorCompra;
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
}