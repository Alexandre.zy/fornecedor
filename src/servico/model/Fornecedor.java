
package servico.model;

import java.util.ArrayList;

/**
 *
 * @author alexandre
 */

public class Fornecedor {
        
    protected String nome;
    protected String telefones;
    private Endereco endereco;
    private ArrayList<Produto> produtos;
        
    public static void main(String[] args) {
        // TODO code application logic here
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefones() {
        return telefones;
    }

    public void setTelefones(String telefones) {
        this.telefones = telefones;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public Fornecedor (String nome, String telefones){
        this.nome = nome;
        this.telefones = telefones;
    }
    
}