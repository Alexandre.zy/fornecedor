/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servico.Controle;


import java.util.ArrayList;
import servico.model.Fornecedor;
import servico.model.PessoaFisica;
import servico.model.PessoaJuridica;
import servico.model.Produto;

/**
 *
 * @author alexandre
 */


public class ControleProduto {
    private final ArrayList<Fornecedor> listaFornecedores;
    private final ArrayList<Produto> listaProdutos;
    
    public ControleProduto (){
        listaFornecedores = new ArrayList<Fornecedor>();
        listaProdutos = new ArrayList<Produto>();
    }
       
    public String adicionar(PessoaFisica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa física adicionado com sucesso";
    }
  
    public String adicionar(PessoaJuridica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso";
    }
    
    public String adicionar (Produto produto){
        listaProdutos.add(produto);
        return "Produto adicionado com sucesso";
    }
    
    public Fornecedor pesquisarNome (String nome){
        for (Fornecedor PessoaFisica : listaFornecedores) {
            if (PessoaFisica.getNome().equalsIgnoreCase(nome)) return PessoaFisica;
        }
        return null;
    }
}