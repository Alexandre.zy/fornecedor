/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servico.Controle;

/**
 *
 * @author alexandre
 */



import java.util.ArrayList;
import servico.model.PessoaFisica;
import servico.model.PessoaJuridica;
import servico.model.Fornecedor;

public class ControleFornecedor {
    private ArrayList<Fornecedor> listaFornecedores;

    public ControleFornecedor () {
        listaFornecedores = new ArrayList<Fornecedor>();
    }
    
    public ArrayList<Fornecedor> getListaFornecedores() {
        return listaFornecedores;
    }

    public void setListaFornecedores(ArrayList<Fornecedor> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }
    
    public String adicionar (PessoaFisica fornecedor) {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Física adicionado com sucesso.";
    }
    
    public String adicionar (PessoaJuridica fornecedor) {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso.";
    }
    
    public void remover (PessoaJuridica fornecedor) {
        listaFornecedores.remove(fornecedor);
    }
    
    public void remover (PessoaFisica fornecedor) {
        listaFornecedores.remove(fornecedor);
    }
    public String pesquisarNome(String umNome) {
        for (Fornecedor umFornecedor: listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)) 
                return "Fornecedor encontrado: " + umFornecedor.getNome();
        }
        return "O fornecedor não foi encontrado";
    }
    
    public Fornecedor Pesquisa(String umNome) {
        for (Fornecedor umFornecedor: listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)) 
                return umFornecedor;
        }
        return null;
    }
    
   public String removeFornecedor(String umNome){
        for (Fornecedor umFornecedor: listaFornecedores) {
            if (umFornecedor.getNome().equalsIgnoreCase(umNome)){
                listaFornecedores.remove(umFornecedor);
                return "Fornecedor removido com sucesso!!!";
            }
        }
        return "O fornecedor não foi encontrado";
    }
}