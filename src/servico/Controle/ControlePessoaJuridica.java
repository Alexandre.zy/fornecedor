/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servico.Controle;

import java.util.ArrayList;
import servico.model.PessoaJuridica;

/**
 *
 * @author alexandre
 */

public class ControlePessoaJuridica {
    
    private ArrayList<PessoaJuridica> listaPessoaJuridica;
    
    public ControlePessoaJuridica () {
        listaPessoaJuridica = new ArrayList<PessoaJuridica>();
    }

    public ArrayList<PessoaJuridica> getListaPessoaJuridica() {
        return listaPessoaJuridica;
    }

    public void setListaPessoaJuridica(ArrayList<PessoaJuridica> listaPessoaJuridica) {
        this.listaPessoaJuridica = listaPessoaJuridica;
    }
    
    
    
    public String adicionarPessoaJuridica (PessoaJuridica umaPessoa) {
        String mensagem = "Pessoa Jurídica, adicionada com sucesso!";
        listaPessoaJuridica.add(umaPessoa);
        return mensagem;
    }
    
    public String removerPessoaJuridica (PessoaJuridica umaPessoa) {
        String mensagem = "Pessoa Jurídica, removida com sucesso!";
        listaPessoaJuridica.remove(umaPessoa);
        return mensagem;
    }
    
    public PessoaJuridica pesquisarPessoaJuridica (String umNome) {
        for (PessoaJuridica umaPessoa : listaPessoaJuridica) {
            if (umaPessoa.getNome().equalsIgnoreCase(umNome)) return umaPessoa;
        }
        return null;
    }
}